import React, {Component} from 'react'
import ComponentE from './ComponentC'
import Inline from './Inline'
import { UserConsumer } from './userContext'

class ComponentF extends Component{
    render()
    {
        return(
           <UserConsumer>
               {
                   (UserName)=>
               {return <div>Hello {UserName}</div>

                   }
               }
           </UserConsumer>
        )
    }
}
export default ComponentF