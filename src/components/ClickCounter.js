import React, { Component } from 'react'
import UpdatedComponent from './withCounter'
import WithCounter from './withCounter';
class ClickCounter extends Component
{ 
  
    render()
    {
        const {count,incrementCount} =this.props
        return <button onClick={this.incrementCount}> {this.props.name} Clicked { count } </button>
    }


}
export default WithCounter(ClickCounter);