import React, { Component } from 'react'
import MemoComp from './MemoProp'

class ParentComp extends Component
{
constructor(props)
{
    super(props)
    this.state = {
        name: 'vishwas'
    }
 }
 ComponentDidMount()
{
    setInterval(()=> this.setState({name:'Vishwas'},2000))
}
render()
{
    return (
        <div>
            Parent Component
            {/* <RegComp name={this.state.name}></RegComp>
            <PureComp name={this.state.name}></PureComp> */}
            <MemoComp name={this.state.name}></MemoComp>
        </div>
    )
}
}

