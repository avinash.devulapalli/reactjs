import React, { Component } from 'react'
import LifeCycleB from './LifeCycleB'

class LifeCycleA extends Component
{
    constructor(props)
    {
        super(props)
        

           this.state={
               name:'vishwas'
           }

           console.log("LifeCycleA constructor")
        

    }

    static getDerivedStateFromProps(props,state)
    {
        console.log("life cycle A getDerived state from props")
        return null

    }
    componentDidMount()
    {
        console.log('LifeCycleA ComponentDidMount')
    }
    render()
    {
        console.log("Life cycle A render")
        return(
            <>
           <p>LifeCycleA</p>
           <LifeCycleB></LifeCycleB>
            </>
        )
    }

}
export default LifeCycleA