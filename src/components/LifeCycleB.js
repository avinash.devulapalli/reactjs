import React, { Component } from 'react'

class LifeCycleB extends Component
{
    constructor(props)
    {
        super(props)
        

           this.state={
               name:'vishwas'
           }

           console.log("LifeCycleB constructor")
        

    }

    static getDerivedStateFromProps(props,state)
    {
        console.log("life cycle A getDerived state from props")
        return null

    }
    componentDidMount()
    {
        console.log('LifeCycleB ComponentDidMount')
    }
    render()
    {
        console.log("Life cycle B render")
        return(
            <>
           <p>LifeCycleB</p>
            </>
        )
    }

}
export default LifeCycleB