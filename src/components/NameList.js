import React from 'react'
import Person from './Person'

function NameList()
{
    // const names=['Bruce','Clark','Diana']
    const persons=[
        {
            id:1,
            name:'Bruce',
            age:20,
            skill:'React'
        },
        {   
            id:2,
            name:'Clark',
            age:20,
            skill:'Angular'

        },
        {
            id:3,
            name:'Diana',
            age:20,
            skill:'Vue'
        }
        
    ]
// const nameList=names.map(name=><h2>{name}</h2>)
const personList=persons.map(person=><Person key={person.id} person={person}></Person>)

return <div>{personList}</div>
}
export default NameList