import React from 'react';


// function Greet(props)
// {
//   const{name,heroName} =props //De-structuring
//      
//     return (
//         <div> <h1>  {props.name} a.k.a  {props.heroName}</h1>
//         {props.children}</div>)
// }

const Greet = ({name, heroName} ) => <h1>Hello Vishwas</h1> 


// export const Greet = ( ) => <h1>Hello Vishwas</h1>


export default Greet




// First step is to import  react

// second step is to return html 

// last step is to  export Greet component and include in App component 

// In App.js import  Greet component from components folder

// to include greet component in app component we  simply specify the component as a custome HTML tag.

//  It is better to use Arow Functions

// By using default export we can import with any name 