import React from  'react';

const Hello=()=>
{
// with using JSX 
    // return(
    //     <div>
    //         <h1>Hello Vishwas</h1>
    //     </div>
    // )
   
// without using JSX 
     return React.createElement('div',{id:'hello', className:'dummyClass'},React.createElement('h1',null,'Hello Vishwas'))

}
export default Hello





// we usually import react when we use JSX because it wants React.createElement();
 