import React, {Component} from 'react';

 class Welcome extends Component
  {
     render()
     {
     return <h1> Welcome {this.props.name} a.k.a {this.props.heroName} </h1>
     }
  } 


export default Welcome;




// First step is to import react  and component class from react.

// Second step we need to extend Component.  

// third class has to implement render method which will return some html.

//  Next step is to export this component and import in App.js component.
