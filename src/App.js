import React from 'react';
import './App.css';
import Greet from './components/Greet';
import Welcome from './components/Welcome';
import Hello from './components/Hello';
import Message from './components/Message';
import Counter from './components/counter';
import FunctionClick from './components/functionClick';
import EventBind from './components/EventBind';
import ParentComponent from './components/ParentComponent';
import UserGreeting from './components/UserGreeting';
import NameList from './components/NameList';
import Form from './components/Form';
import LifeCycleA from './components/LifeCycleA';
import Inline from './components/Inline';
import  './components/appStyles.css';
import styles from'./components/appStyles.module.css'
import ClickCounter from './components/ClickCounter';
import HoverCounter from './components/HoverCounter';
import ClickCounterTwo from './components/ClickCounterTwo';
import HoverCounterTwo from './components/HoverCounterTwo';
import User from './components/User';
import CounterTwo from './components/CounterTwo';
import ComponentC from './components/ComponentC'
import { UserProvider } from './components/userContext';
// import UserProvider from './components/userContext'


function App() {
  //with jsx
  return (
    
    <div className="App">
      <h1 className='error'> Error </h1>
    <h1 className={styles.success}></h1>
      {/* props  */}
      <Form></Form>
      <Greet name="Bruce" heroName="Batman">
        {/* child props */}
        <p>This is children props </p>
      </Greet>
      <Greet name="Clark" heroName="SuperMan">
        <button>submit</button>
      </Greet>
    
      <Welcome name="Bruce" heroName="Batman"></Welcome>
      <Welcome name="Clark" heroName="superMan"></Welcome>
      <Hello></Hello>
      <Message ></Message>
      <Counter></Counter>
      <FunctionClick></FunctionClick>
     <EventBind></EventBind>
     <ParentComponent></ParentComponent>
    <UserGreeting></UserGreeting>
    <NameList></NameList>
    <Inline></Inline>
    <LifeCycleA></LifeCycleA>
    <ParentComponent></ParentComponent>
    <ClickCounter></ClickCounter>   
    <HoverCounter></HoverCounter> 
    {/* <ClickCounterTwo></ClickCounterTwo>
    <HoverCounterTwo></HoverCounterTwo> */}
    <CounterTwo render={(count,incrementCount)=><ClickCounterTwo count={count} incrementCount={incrementCount}/>}></CounterTwo>
    <CounterTwo render={(count,incrementCount)=><HoverCounterTwo count={count} incrementCount={incrementCount}/>}></CounterTwo>
    <ComponentC></ComponentC>
    <User name={()=>'vishwas'}></User>
    <UserProvider value="vishwas">
      <ComponentC/>
    </UserProvider>
    </div>
  );
  // without jsx
  // return React.createElement('div', { id:'hello',className:'dummy'},React.createElement('h1',null, 'Hello Vishwas'))
}

export default App;
